import copy


def gerar_matriz_aumentada(matriz, vetor_b):
    matriz_aumentada = copy.deepcopy(matriz)
    copia_vetor_b = copy.deepcopy(vetor_b)

    for linha in range(0,len(matriz)):
        matriz_aumentada[linha].append(copia_vetor_b[linha])

    return matriz_aumentada


def exibir_matriz(matriz):
    print("")
    for linha in range(0, len(matriz)):
        print("  |", end="")
        for coluna in range(0, len(matriz[linha])):
            print(" {0:>10.6f}".format(matriz[linha][coluna]), end="")
        print(" |", end="")
        print("")
    print("")


def exibir_matriz_aumentada(matriz):
    for linha in range(0, len(matriz)):
        print("  |", end="")
        for coluna in range(0, len(matriz[linha])):
            if coluna == (len(matriz[linha])-1):
                print(" :{0:>10.6f}".format(matriz[linha][coluna]), end="")
            else:
                print(" {0:>10.6f}".format(matriz[linha][coluna]), end="")

        print("|")
    print("")


def Determinante(matriz):


    if len(matriz) == 2: # matriz de ordem 2
        solucao = (matriz[0][0] * matriz[1][1]) - (matriz[1][0] * matriz[0][1])
    else:
        if len(matriz) > 2 and len(matriz) < 4: # matriz de ordem 3

            soma_diagonal_principal = (matriz[0][0] * matriz[1][1] * matriz[2][2]) + (matriz[0][1] * matriz[1][2] * matriz[2][0]) + (matriz[0][2] * matriz[1][0] * matriz[2][1])

            soma_daigonal_secundaria = (matriz[2][0] * matriz[1][1] * matriz[0][2]) + (matriz[2][1] * matriz[1][2] * matriz[0][0]) + (matriz[2][2] * matriz[1][0] * matriz[0][1])

            solucao = soma_diagonal_principal - soma_daigonal_secundaria

        else: # matriz de ordem maior que 3
            print("Não trabalho com matriz de ordem maior que 3")
            solucao = 0

    return solucao
