import extras
import copy


def problema():
    matriz = [[10, 2, 1],
              [ 1, 5, 1],
              [2, 3, 10]]

    vetor_b = [7, -8, 6]

    solucao_inicial = [0.7, -1.6, 0.6]

    epsilon = 0.05

    limite_iteracao = 50

    matriz_aumentada = extras.gerar_matriz_aumentada(matriz, vetor_b)

    metodo_gaus_seidel(matriz_aumentada, solucao_inicial, epsilon, limite_iteracao)



def metodo_gaus_seidel(matriz_aumentada, solucao_inicial, epsilon, limite_iteracao):
    Continuar = True
    iterecao = 0
    condicao = []

    cabecalho(matriz_aumentada, epsilon, solucao_inicial)
    #passo 0
    condicao = [True, "Não", "Não", "Não", "-", "-"]
    passos(solucao_inicial, condicao, iterecao)

    # processo de itercao 1
    solucao_anterior = copy.copy(solucao_inicial)
    solucao_atual = calculo_nova_solucao(matriz_aumentada, solucao_anterior)
    iterecao = iterecao +1

    condicao = condicao_parada(solucao_anterior, solucao_atual, iterecao, epsilon, limite_iteracao)
    passos(solucao_atual, condicao, iterecao)
    continuar = condicao[0]

    while continuar: # True continua e False para

        solucao_anterior = copy.copy(solucao_atual)
        solucao_atual = calculo_nova_solucao(matriz_aumentada, solucao_anterior)
        iterecao = iterecao +1

        condicao = condicao_parada(solucao_anterior, solucao_atual, iterecao, epsilon, limite_iteracao)
        passos(solucao_atual, condicao, iterecao)
        continuar = condicao[0]

    print("")
    print("")



def calculo_nova_solucao(matriz, solucao_anterior):
    nova_solucao_atual = []
    ordem = len(matriz) # numero de linhas e colunas para matriz nxn sem contar o vetor_b na ultima coluna (n+1)

    for linha in range(ordem):
        somatorio = 0
        for coluna in range(ordem):
            if coluna != linha:
                if coluna < linha:
                    somatorio = somatorio + (matriz[linha][coluna] * nova_solucao_atual[coluna])
                else:
                    somatorio = somatorio + (matriz[linha][coluna] * solucao_anterior[coluna])

        X_i = (matriz[linha][ordem] - somatorio)/ matriz[linha][linha]
        nova_solucao_atual.append(copy.copy(X_i))

    return nova_solucao_atual


def condicao_parada(solucao_anterior, solucao_atual, iterecao, epsilon, limite_iteracao):
    saidas = []
    continua = True
    condicao_1 = "Não"
    condicao_2 = "Não"
    condicao_3 = "Não"


    # condica 1: atingir a precissao desejada
    distancia = round(maximo_diferenca(solucao_atual, solucao_anterior), 6)
    if distancia < epsilon:
        continua = False
        condicao_1 = "Sim"

    # criterio 2: erro relativo
    distancia_relativa = round(maximo_diferenca(solucao_atual, solucao_anterior) / maximo(solucao_atual), 6)
    if distancia_relativa < epsilon:
        continua = False
        condicao_2 = "Sim"

    # Condicao 3: numero maximo de iteracoes
    if iterecao == limite_iteracao:
        continuar = False
        condicao_3 = "Sim"

    # retorna um vetor que informa se é para continuar ou parar com as iteracoes
    # e a condicao que determinou a parada resebe o valor "Sim" e as demais recebem "Não"
    # desse modo da pra saber por qual condicao parou na hora de exibir os resultados
    saidas.append(continua)
    saidas.append(condicao_1)
    saidas.append(condicao_2)
    saidas.append(condicao_3)
    saidas.append(distancia)
    saidas.append(distancia_relativa)
    return saidas


def maximo_diferenca(solucao_atual, solucao_anterior):
    vetor_diferenca = []

    # gera o vetor com o modulo da diferenca entre solucao_atual e solucao_anterior | solucao_atual - solucao_anterior |
    for indice in range(len(solucao_atual)):
        diferenca_absoluta = abs(solucao_atual[indice] - solucao_anterior[indice])
        vetor_diferenca.append(copy.copy(diferenca_absoluta))


    # verifica qual é o maior
    maior = vetor_diferenca[0]
    for indice in range(1, len(vetor_diferenca)):
        if vetor_diferenca[indice] > maior:
            maior = vetor_diferenca[indice]

    return maior


def maximo(solucao):
    maior = abs(solucao[0])

    for indice in range(1, len(solucao)):
        if abs(solucao[indice]) > maior:
            maior = abs(solucao[indice])

    return maior


def cabecalho(matriz, epsilon, solucao_inical):
    print("")
    print("")
    extras.exibir_matriz_aumentada(matriz)
    print("")
    print("     Epsilon: {0}            Solução Inicial: {1}".format(epsilon, solucao_inical))
    print("")
    print("")
    print("  ------------------------------------------------------------ Gauss Seidel -------------------------------------------------------------")
    print("")
    print("   i                 Solução                 distancia(d)   distancia relativa(dr)   d < epsilon    dr < epsilon   nº maximo de iterações")
    print("  ---------------------------------------------------------------------------------------------------------------------------------------")


def passos(solucao_atual,condicao, iteracao):
    solucao = copy.copy(solucao_atual)

    parada_1 = condicao[1]
    parada_2 = condicao[2]
    parada_3 = condicao[3]
    distancia = condicao[4]
    distancia_relativa = condicao[5]

    print(" {0:>3}    [".format(iteracao), end="")

    for indice in range(len(solucao)):
        print("{0:10.6f}".format(solucao[indice]), end="")
    print(" ]", end="")

    print("    {0:^12}   {1:^22}   {2:^11}    {3:^12}   {4:^22}".format(distancia, distancia_relativa, parada_1, parada_2, parada_3))

    print("  _______________________________________________________________________________________________________________________________________")

    print("")
