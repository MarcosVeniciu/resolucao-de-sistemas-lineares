import pivoteamento_parcial
import eliminacao_de_gauss
import fatoracao_sholesky
import matriz_triangular
import fatoracao_lu
import gauss_jacobi
import gauss_seidel
import os


os.system('clear') or None #limpar a tela sempre que for executado

print(" 1 - Resolver Matriz Triangular")
print(" 2 - Eliminação de Gauss")
print(" 3 - Fatoração L.U.")
print(" 4 - Fatoração de Sholesky")
print(" 5 - Método de Gauss-Jacobi")
print(" 6 - Método de Gauss-Seidel")

metodo = int(input())
# metodo = 4

os.system('clear') or None
if metodo == 1 : matriz_triangular.definir_problema()
if metodo == 2 : eliminacao_de_gauss.problema()
if metodo == 3 : fatoracao_lu.problema()
if metodo == 4 : fatoracao_sholesky.problema()
if metodo == 5 : gauss_jacobi.problema()
if metodo == 6 : gauss_seidel.problema()
