import copy
import extras

def problema():

    vetor_b = [2, 5, 4]

    matriz = [[5, -4, 3],
              [3,  2, 5],
              [6,  3, 1]]



    matriz_aumentada = extras.gerar_matriz_aumentada(matriz, vetor_b)

    print("   inicial")
    extras.exibir_matriz_aumentada(matriz_aumentada)
    print("")
    print("")


    for pivo in range(0, len(matriz_aumentada)): # o pivo representa a linha e coluna na diagonal princial da matriz
        matriz_aumentada = pivoteamento(pivo, matriz_aumentada)
        print("   {0}ª pivo".format(pivo+1))
        extras.exibir_matriz_aumentada(matriz_aumentada)
    print("")


def pivoteamento(pivo, matriz_aumentada): # o pivo esta na diagonal principal, ou seja, a linha e a coluna tem o mesmo indice na matriz
    matriz_pivoteada = copy.deepcopy(matriz_aumentada)
    linha = pivo
    coluna = pivo

    maior = matriz_pivoteada[linha][coluna]
    linha_pivo = linha

    cont = linha +1
    while cont < len(matriz_pivoteada):
        if abs(matriz_pivoteada[cont][coluna]) > maior:
            maior = matriz_pivoteada[cont][coluna]
            linha_pivo = cont
        cont = cont +1


    if linha_pivo != linha: # troca a linha pivo com a nova linha pivo
        for C in range(0, len(matriz_pivoteada[linha_pivo])):
            # salva o que esta na linha do pivo
            temporario = matriz_pivoteada[linha][C]
            # faz a linha do pivo receber a nova linha pivo
            matriz_pivoteada[linha][C] = matriz_pivoteada[linha_pivo][C]
            # salva o que estava na linha pivo na linha em que estava o novo pivo
            matriz_pivoteada[linha_pivo][C] = temporario


    return matriz_pivoteada
