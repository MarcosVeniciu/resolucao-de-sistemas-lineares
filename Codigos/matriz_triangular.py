import extras


def definir_problema():

    matriz = [[5, 2, 3],
              [3, -2, 5],
              [6, 3, 1]]


    vetor_b = [1, 2, 3]


    matriz_aumentada = extras.gerar_matriz_aumentada(matriz, vetor_b)
    solucao = Resolver_Matriz(matriz_aumentada)

    print("")
    print("")
    extras.exibir_matriz_aumentada(matriz_aumentada)
    print("")
    print("")
    print(" Solucao: {0}".format(solucao))
    print("")
    print("")



def Resolver_Matriz(matriz):
    vetor_solucao = []



    # Verifica se é traingular superior ou inferior ou diagonal
    if matriz_diagonal(matriz):
        print("A matriz é triangular superior e inferior.")

    else:
        if Triangular_Superior(matriz):
            vetor_solucao = Solucionar_Triangular_Superior(matriz)

        else:
            if triangular_Inferior(matriz):
                vetor_solucao = Solucionar_Triangular_Inferior(matriz)

            else:
                print("A matriz não é triangular.")


    return vetor_solucao


def Triangular_Superior(matriz):
    trangular_superior = True

    for linha in range(1, len(matriz)):
        for coluna in range(linha):
            if matriz[linha][coluna] != 0:
                trangular_superior = False
                break # para sair do segundo for, o das colunas
        if trangular_superior == False: # pra sair do primeiro for o das linhas
            break

    return trangular_superior


def triangular_Inferior(matriz):
    triangular_inferior = True
    ordem = len(matriz) # numero de linhas da matriz nxn, onde tem uma coluna a mais que é o vetor b

    for linha in range(ordem-1):
        for coluna in range((linha+1), ordem):
            if matriz[linha][coluna] != 0:
                triangular_inferior = False
                break # para sair do segundo for, o das colunas
        if triangular_inferior == False: # pra sair do primeiro for o das linhas
            break
    return triangular_inferior


def matriz_diagonal(matriz):
    diagonal = False

    if Triangular_Superior(matriz) and triangular_Inferior(matriz):
        diagonal = True

    return diagonal


def Solucionar_Triangular_Superior(matriz):
    vetor_x = []
    ordem = len(matriz)

    X_n = matriz[ordem-1][ordem] / matriz[ordem-1][ordem-1]
    vetor_x.insert(0, X_n)

    for linha in range(ordem-2, -1, -1):
        soma = 0
        for coluna in range(linha+1, ordem):
            indice_vetor_x = coluna - (linha+1)
            soma = soma + (matriz[linha][coluna] * vetor_x[indice_vetor_x])

        X_linha = (matriz[linha][ordem] - soma)/ matriz[linha][linha] # X[2] = b[2] - somatorio da linha 2 / matriz[2][2]
        vetor_x.insert(0, X_linha) # inseri no inicio do vetor pois esta indo da linha n ate 1


    return vetor_x



def Solucionar_Triangular_Inferior(matriz):
    vetor_x = []
    ordem = len(matriz) # representa o numero de linha da matriz nxn, mais também serve para indicar a coluna do vetor b, pois se é uma matriz 3x3 o indice vai de 0 a 2 e o vetor b seria a coluna 4 com indice 3

    X_1 = matriz[0][ordem] / matriz[0][0]
    vetor_x.append(X_1)


    for linha in range(1, ordem):
        soma = 0
        for coluna in range(0, linha):
            soma = soma + (matriz[linha][coluna] * vetor_x[coluna])

        X_linha = (matriz[linha][ordem] - soma)/ matriz[linha][linha] # X_linha é o valor do vetor solucao x no indice linha
        vetor_x.append(X_linha)


    return vetor_x
