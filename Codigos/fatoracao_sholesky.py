import matriz_triangular
import extras
import copy
import math

def problema():

    matriz = [[4, 12, -16],
              [12, 37, -43],
              [-16, -43, 98]]


    vetor_b = [1, 2, 3]



    Fatoracao_Sholesky(matriz, vetor_b)

    print("")
    print("")


def Fatoracao_Sholesky(matriz_A, vetor_b):
    if matriz_simetrica(matriz_A) == True:
        if definida_positiva(matriz_A) == True:

            cabecalho()
            print("  Matriz A")
            extras.exibir_matriz(matriz_A)
            matriz_G = gerar_matriz_G(matriz_A) # gera a matriz G apartir da matriz A
            print("  matriz G")
            extras.exibir_matriz(matriz_G)
            matriz_G_transposta = transpor_matriz(matriz_G) # gera uma matriz transposta de G
            print("  matriz G transposta")
            extras.exibir_matriz(matriz_G_transposta)

            # gera uma matriz aumentada da matriz G com o vetor b
            matriz_G_aumentada = extras.gerar_matriz_aumentada(matriz_G, vetor_b)
            # gera o vetor solucao y
            vetor_y = matriz_triangular.Resolver_Matriz(matriz_G_aumentada)
            exibir_sistema_inferior(matriz_G_aumentada, vetor_y) # G.y = b


            # gera uma matriz aumentada da matriz G transposta com o vetor y
            matriz_G_transposta_aumentada = extras.gerar_matriz_aumentada(matriz_G_transposta, vetor_y)
            # gera o vetor solucao x
            vetor_x = matriz_triangular.Resolver_Matriz(matriz_G_transposta_aumentada)
            exibir_sistema_superior(matriz_G_transposta_aumentada, vetor_x) # G_transposta.X = y
        else:
            print("a matriz A não é definida positiva")
    else:
        print("A matriz A não é simetrica")

    print("")


def matriz_simetrica(matriz_A):
    simetria = True
    for i in range(len(matriz_A)):# percorre as linhas da matriz
        for j in range(i+1, len(matriz_A)): # percorre as colunas da matriz
            if matriz_A[i][j] != matriz_A[j][i]: # verifica se a Aij = Aji
                simetria = False
                break
        if simetria == False:
            break

    return simetria


def definida_positiva(matriz):
    positiva = True
    for cont in range(len(matriz)):
        # gerar submatriz
        submatriz = gerar_submatriz(matriz, cont+1)

        if cont == 0:
            determinante = submatriz
        else:
            determinante = extras.Determinante(submatriz)

        if(determinante < 0):
            positiva = False
            break

    return positiva


def gerar_submatriz(matriz, ordem):
    nova_matriz = []
    nova_linha = []

    if ordem == 1: # matriz com apenas um elemento
        nova_matriz = copy.copy(matriz[0][0])
    else: # para matriz de ordem maior que 2
        for linha in range(ordem):
            for coluna in range(ordem):
                nova_linha.append(matriz[linha][coluna])
            nova_matriz.append(copy.copy(nova_linha))
            nova_linha.clear()

    return nova_matriz


def gerar_matriz_G(matriz_A):
    G = []
    linha_G = []

    # Gera uma matriz G onde todos os valores valem zero
    for linha in range(len(matriz_A)):
        for coluna in range(len(matriz_A)):
            linha_G.append(0)
        G.append(copy.copy(linha_G))
        linha_G.clear()

    print("  ------------------------------------------------------------------------------------------------------------------------------------------")
    # Calculas os valores da matriz G da primeira coluna (coluna 0)
    G[0][0] = math.sqrt(matriz_A[0][0])
    print("  G[1][1] = raiz_quadrada({0}) = {1:<10.6f}".format(matriz_A[0][0], G[0][0]))

    for linha in range(1, len(G)):
        G[linha][0] = matriz_A[linha][0] / G[0][0]
        print("  G[{0}][1] = {1} / {2} = {3:<10.6f}".format(linha+1, matriz_A[linha][0], G[0][0], G[linha][0]))


    # para as demais colunas da matriz G (colunas de 1 ate n)
    for coluna in range(1, len(G)):
        for linha in range(coluna, len(G)):
            somatorio = 0
            if linha == coluna: # diagonal principal
                print("  G[{0}][{1}] = raiz_quadrada( {2} - ( ".format(linha+1, coluna+1, matriz_A[linha][linha]), end="")
                for k in range(linha):# para percorrer as colunas de uma linha especifica
                    somatorio = somatorio + math.pow(G[linha][k], 2)
                    if k == linha-1:
                        print("({0})^2 ))".format(G[linha][k]), end="")
                    else:
                        print("({0})^2 + ".format(G[linha][k]), end="")

                G[linha][linha] = math.pow((matriz_A[linha][linha] - somatorio), (1/2))
                print(" = {0:<10.6f}".format(G[linha][linha]))
            else:
                print("  G[{0}][{1}] = {2:<10.6f} - ( ".format(linha+1, coluna+1, matriz_A[linha][coluna]), end="")
                for  k in range(coluna):
                    somatorio = somatorio + (G[linha][k] * G[coluna][k])
                    if k == coluna-1:
                        print("({0} * {1}) ))".format(G[linha][k], G[coluna][k]), end="")
                    else:
                        print("({0} * {1}) + ".format(G[linha][k], G[coluna][k]), end="")

                G[linha][coluna] = (matriz_A[linha][coluna] - somatorio)/ G[coluna][coluna]
                print(" / {0} = {1:<10.6f}".format(G[coluna][coluna], G[linha][coluna]))

    print("  ------------------------------------------------------------------------------------------------------------------------------------------")
    return G


def transpor_matriz(G):
    transposta = []
    linha_transposta = []

    for coluna in range(len(G)):
        for linha in range(len(G)):
            linha_transposta.append(copy.copy(G[linha][coluna]))
        transposta.append(copy.copy(linha_transposta))
        linha_transposta.clear()

    return transposta


def cabecalho():
    print("")
    print("  ------------------------------------------------------------ fatoração de Sholsky -------------------------------------------------------------")


def exibir_sistema_inferior(matriz, solucao):
    ordem = len(matriz)
    print("  ------------------------------------------------------------------------------------------------------------------------------------------")
    print("  Resolução do Sistema G.y = b")
    print("")
    for linha in range(ordem):
        for coluna in range(ordem):
            if coluna > linha:
                if coluna == ordem-1:
                    print("{0:^14}= {1:<10.6f}".format(" ", matriz[linha][ordem]), end="")
                else:
                    print("{0:^14}".format(" "), end="")
            else:
                if coluna == ordem-1:
                    if matriz[linha][coluna] > 0:
                        print("{0:+10.6f}Y_{1} = {2:<10.6f}".format(matriz[linha][coluna], coluna+1, matriz[linha][ordem]), end="")
                    else:
                        print("{0:10.6f}Y_{1} = {2:<10.6f}".format(matriz[linha][coluna], coluna+1, matriz[linha][ordem]), end="")

                else:
                    if matriz[linha][coluna] > 0 and coluna > 0:
                        print("{0:+10.6f}Y_{1} ".format(matriz[linha][coluna], coluna+1), end="")
                    else:
                        print("{0:10.6f}Y_{1} ".format(matriz[linha][coluna], coluna+1), end="")
        print("")
    print("")
    print("  Solução: ", end="")
    for indice in range(len(solucao)):
        print("Y_{0} ={1:10.6f}   ".format(indice+1, solucao[indice]), end="")
    print("")


def exibir_sistema_superior(matriz, solucao):
    ordem = len(matriz)
    print("  ------------------------------------------------------------------------------------------------------------------------------------------")
    print("  Resolução do Sistema G_transposta.x = y")
    print("")
    for linha in range(ordem):
        for coluna in range(ordem):
            if coluna < linha:
                print("{0:^14}".format(" "), end="")
            else:
                if coluna == ordem-1:
                    if matriz[linha][coluna] > 0:
                        print("{0:+10.6f}X_{1} = {2:<10.6f}".format(matriz[linha][coluna], coluna+1, matriz[linha][ordem]), end="")
                    else:
                        print("{0:10.6f}X_{1} = {2:<10.6f}".format(matriz[linha][coluna], coluna+1, matriz[linha][ordem]), end="")

                else:
                    if matriz[linha][coluna] > 0 and coluna > 0:
                        print("{0:+10.6f}X_{1} ".format(matriz[linha][coluna], coluna+1), end="")
                    else:
                        print("{0:10.6f}X_{1} ".format(matriz[linha][coluna], coluna+1), end="")
        print("")
    print("")
    print("  Solução: ", end="")
    for indice in range(len(solucao)):
        print("X_{0} ={1:10.6f}   ".format(indice+1, solucao[indice]), end="")
    print("")
