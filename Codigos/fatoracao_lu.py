import matriz_triangular
import extras
import copy

def problema():
    pivo_parcial = False # Use True se for querer com pivoteamento parcial e False caso não queira usa-lo

    matriz = [[5, 2, 3],
              [3, -2, 5],
              [6, 3, 1]]


    vetor_b = [1, 2, 3]


    matriz_aumentada = extras.gerar_matriz_aumentada(matriz, vetor_b)

    fatoracao_lu(matriz_aumentada, pivo_parcial)



def fatoracao_lu(matriz_A, pivo_parcial):
    ordem = len(matriz_A)
    matriz_A_linha = []
    matriz_L = matriz_l(ordem) # gera uma matriz para ser inserido os multiplos
    matriz_U = []
    vetor_b = []
    vetor_y = []
    solucao = []

    cabecalho()

    matriz_A_linha = matriz_l_u(matriz_L, matriz_A, pivo_parcial)
    matriz_U = matriz_u(matriz_A_linha)# copia tudo exeto a ultima coluna
    vetor_b = vetor_B(matriz_A_linha)# copia apenas a ultima coluna



    # ja tenho a matriz l e U e o vetor b
    print("")
    print("  Matriz A")
    print("")
    extras.exibir_matriz_aumentada(matriz_A)
    print("")
    print("  Matriz L")
    extras.exibir_matriz(matriz_L)
    print("")
    print("  Matriz U")
    extras.exibir_matriz(matriz_U)


    matriz_L = extras.gerar_matriz_aumentada(matriz_L, vetor_b)
    vetor_y = matriz_triangular.Resolver_Matriz(matriz_L)

    exibir_sistema_inferior(matriz_L, vetor_y)


    matriz_U = extras.gerar_matriz_aumentada(matriz_U, vetor_y)
    solucao_X = matriz_triangular.Resolver_Matriz(matriz_U)

    exibir_sistema_superior(matriz_U, solucao_X)

    print("")
    print("")


# parte da Eliminação de Gauss
def matriz_l_u(matriz_L, matriz_A, pivo_parcial):
    matriz_A_linha = copy.deepcopy(matriz_A)
    ordem = len(matriz_A)


    for coluna in range(ordem):
        if (matriz_A_linha[coluna][coluna] == 0) or (pivo_parcial == True):
            matriz_A_linha = pivoteamento_parcial.pivoteamento(coluna, matriz_A_linha)
        pivo = matriz_A_linha[coluna][coluna]
        linha_pivo = coluna

        for linha in range(coluna+1, ordem):
            multiplo = matriz_A_linha[linha][coluna] / pivo
            matriz_L[linha][coluna] = multiplo
            matriz_A_linha = multiplicar_linha(matriz_A_linha, linha, linha_pivo, multiplo)

    return matriz_A_linha


def multiplicar_linha(matriz, linha, linha_pivo, multiplicador):
    matriz_multiplicada = copy.deepcopy(matriz)
    for coluna in range(len(matriz[linha])-1):
        novo_valor = matriz_multiplicada[linha][coluna] - (multiplicador * matriz[linha_pivo][coluna])
        matriz_multiplicada[linha][coluna] = round(novo_valor, 6)# numero de casas decimais

    return matriz_multiplicada


def matriz_l(ordem):
    matriz = []
    linha_matriz = []
    for linha in range(ordem):
        for coluna in range(ordem):
            if linha == coluna:
                linha_matriz.append(1)
            else:
                linha_matriz.append(0)
        matriz.append(copy.copy(linha_matriz))
        linha_matriz.clear()
    return matriz


def matriz_u(matriz):
    matriz_U = []
    linha_matriz = []

    for linha in range(len(matriz)):
        for coluna in range(len(matriz)):
            linha_matriz.append(copy.copy(matriz[linha][coluna]))
        matriz_U.append(copy.copy(linha_matriz))
        linha_matriz.clear()

    return matriz_U


def vetor_B(matriz):
    ordem = len(matriz)
    vetor_b = []

    for linha in range(ordem):
        vetor_b.append(matriz[linha][ordem])

    return vetor_b

def cabecalho():
    print("")
    print("  ------------------------------------------------------------ Eliminação L.U. -------------------------------------------------------------")


def exibir_sistema_superior(matriz, solucao):
    ordem = len(matriz)
    print("  ------------------------------------------------------------------------------------------------------------------------------------------")
    print("  Resolução do Sistema U.x = y")
    print("")
    for linha in range(ordem):
        for coluna in range(ordem):
            if coluna < linha:
                print("{0:^14}".format(" "), end="")
            else:
                if coluna == ordem-1:
                    if matriz[linha][coluna] > 0:
                        print("{0:+10.6f}X_{1} = {2:<10.6f}".format(matriz[linha][coluna], coluna+1, matriz[linha][ordem]), end="")
                    else:
                        print("{0:10.6f}X_{1} = {2:<10.6f}".format(matriz[linha][coluna], coluna+1, matriz[linha][ordem]), end="")

                else:
                    if matriz[linha][coluna] > 0 and coluna > 0:
                        print("{0:+10.6f}X_{1} ".format(matriz[linha][coluna], coluna+1), end="")
                    else:
                        print("{0:10.6f}X_{1} ".format(matriz[linha][coluna], coluna+1), end="")
        print("")
    print("")
    print("  Solução: ", end="")
    for indice in range(len(solucao)):
        print("X_{0} ={1}   ".format(indice+1, solucao[indice]), end="")
    print("")


def exibir_sistema_inferior(matriz, solucao):
    ordem = len(matriz)
    print("  ------------------------------------------------------------------------------------------------------------------------------------------")
    print("  Resolução do Sistema L.y = b")
    print("")
    for linha in range(ordem):
        for coluna in range(ordem):
            if coluna > linha:
                if coluna == ordem-1:
                    print("{0:^14}= {1:<10.6f}".format(" ", matriz[linha][ordem]), end="")
                else:
                    print("{0:^14}".format(" "), end="")
            else:
                if coluna == ordem-1:
                    if matriz[linha][coluna] > 0:
                        print("{0:+10.6f}Y_{1} = {2:<10.6f}".format(matriz[linha][coluna], coluna+1, matriz[linha][ordem]), end="")
                    else:
                        print("{0:10.6f}Y_{1} = {2:<10.6f}".format(matriz[linha][coluna], coluna+1, matriz[linha][ordem]), end="")

                else:
                    if matriz[linha][coluna] > 0 and coluna > 0:
                        print("{0:+10.6f}Y_{1} ".format(matriz[linha][coluna], coluna+1), end="")
                    else:
                        print("{0:10.6f}Y_{1} ".format(matriz[linha][coluna], coluna+1), end="")
        print("")
    print("")
    print("  Solução: ", end="")
    for indice in range(len(solucao)):
        print("Y_{0} ={1:10.6f}   ".format(indice+1, solucao[indice]), end="")
    print("")
