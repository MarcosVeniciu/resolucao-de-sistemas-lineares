import pivoteamento_parcial
import matriz_triangular
import copy
import extras
# site pra testar calcular a eliminação de gauss com 6 casas decimais : https://pt.planetcalc.com/3571/?thanks=1
def problema():

    pivo_parcial = False # Use True se for querer com pivoteamento parcial e False caso não queira usa-lo

    matriz = [[5, 2, 3],
              [3, -2, 5],
              [6, 3, 1]]


    vetor_b = [1, 2, 3]


    matriz_aumentada = extras.gerar_matriz_aumentada(matriz, vetor_b)

    eliminacao_gauss(matriz_aumentada, pivo_parcial)



def eliminacao_gauss(matriz_aumentada, pivo_parcial):
    matriz = copy.deepcopy(matriz_aumentada)
    ordem = len(matriz) # o tamanho da matriz é o numero de linhas, pois nas colunas tem o vetor b junto
    iteracao = 0
    cabecalho()


    exibir_passos(matriz, iteracao) #iteracao zero
    for coluna in range(ordem):

        if (matriz[coluna][coluna] == 0) or (pivo_parcial == True): # verifica se vai usar ou não o pivoteamento parcial
            matriz = pivoteamento_parcial.pivoteamento(coluna, matriz)
        pivo = matriz[coluna][coluna]

        linha_pivo = coluna

        for linha in range(coluna+1, ordem):

            multiplo = matriz[linha][coluna] / pivo
            matriz = multiplicar_linha(matriz, linha, linha_pivo, multiplo)
        iteracao = iteracao +1
        if coluna != ordem-1:
            exibir_passos(matriz, iteracao)

    solucao = matriz_triangular.Resolver_Matriz(matriz)
    exibir_sistema_superior(matriz, solucao)


    print("")
    print("")


def multiplicar_linha(matriz, linha, linha_pivo, multiplicador):
    matriz_multiplicada = copy.deepcopy(matriz)

    for coluna in range(len(matriz[linha])):
        novo_valor = matriz_multiplicada[linha][coluna] - (multiplicador * matriz[linha_pivo][coluna])
        matriz_multiplicada[linha][coluna] = round(novo_valor, 6)# numero de casas decimais

    return matriz_multiplicada


def cabecalho():
    print("")
    print("  ------------------------------------------------------------ Eliminação de Gauss -------------------------------------------------------------")
    print("")


def exibir_passos(matriz, iteracao):

    print("  Passo {0}".format(iteracao))
    print("")
    print("")
    coluna = iteracao
    for linha in range((iteracao+1), len(matriz)):
        m = matriz[linha][coluna] / matriz[coluna][coluna]

        print("  M[{0}][{1}] = {2:>10.6f} / {3:<10.6f}= {4:<10.6f}   ".format(linha+1, coluna+1, matriz[linha][coluna], matriz[coluna][coluna], m), end="")
        if linha == (len(matriz)-1):
            print("")
            print("")
            print("")

    for linha in range(0, len(matriz)):
        print("  |", end="")
        for coluna in range(0, len(matriz[linha])):
            if coluna == (len(matriz[linha])-1):
                print(" :{0:>10.6f}".format(matriz[linha][coluna]), end="")
            else:
                print(" {0:>10.6f}".format(matriz[linha][coluna]), end="")

        if linha > iteracao:
            print("| L{0} = L{0} - ( M[{0}][{1}] * L{1} )".format(linha+1, iteracao+1))
        else:
            print("|")
    print("")
    print("  ----------------------------------------------------------------------------------------------------------------------------------------------")


def exibir_sistema_superior(matriz, solucao):
    ordem = len(matriz)

    print("  Resolução do Sistema")
    print("")
    for linha in range(ordem):
        for coluna in range(ordem):
            if coluna < linha:
                print("{0:^14}".format(" "), end="")
            else:
                if coluna == ordem-1:
                    if matriz[linha][coluna] > 0:
                        print("{0:+10.6f}X_{1} = {2:<10.6f}".format(matriz[linha][coluna], coluna+1, matriz[linha][ordem]), end="")
                    else:
                        print("{0:10.6f}X_{1} = {2:<10.6f}".format(matriz[linha][coluna], coluna+1, matriz[linha][ordem]), end="")

                else:
                    if matriz[linha][coluna] > 0 and coluna > 0:
                        print("{0:+10.6f}X_{1} ".format(matriz[linha][coluna], coluna+1), end="")
                    else:
                        print("{0:10.6f}X_{1} ".format(matriz[linha][coluna], coluna+1), end="")
        print("")
    print("")
    print("  Solução: ", end="")
    for indice in range(len(solucao)):
        print("X_{0} = {1}   ".format(indice+1, solucao[indice]), end="")
    print("")
